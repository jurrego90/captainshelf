import numpy as np


BEAUFORT_SCALE = [
    [0, "Calm", 0, 1],
    [1, "Light Air", 1, 3.5],
    [2, "Light Breeze", 3.5, 6.5],
    [3, "Gentle Breeze", 6.5, 10.5],
    [4, "Moderate Breeze", 10.5, 16.5],
    [5, "Fresh Breeze", 16.5, 21.5],
    [6, "Strong Breeze", 21.5, 27.5],
    [7, "Near Gale", 27.5, 33.5],
    [8, "Gale", 33.5, 40.5],
    [9, "Strong Gale", 40.5, 47.5],
    [10, "Storm", 47.5, 55.5],
    [11, "Violent Storm", 55.5, 63.5],
    [12, "Hurricane", 63.5, np.inf],
]



def knots_to_beaufort(speed_knots: float)->int:
    """ 
    Returns the Beaufort scale rating based on the wind speed
    in knots.
    """
    for el in BEAUFORT_SCALE:
        if speed_knots <= el[3]:
            return el[0]