from typing import Union
import pandas as pd
from tqdm import tqdm

from ..base_transformers import BaseIngestionTransformer


class CoordsTransformer(BaseIngestionTransformer):
    def __init__(self, **params):
        self.format = params.get('format', 'degree_str')
        self.coord_cols = params.get('coord_cols', [])
        self.to_dict = params.get('to_dict', False)
        self.cardinal_signs = {'n': 1, 'e': 1, 'w': -1, 's': -1}


    def transform(self, data: Union[pd.DataFrame, list[dict]]):
        df = self.convert_degrees_str(pd.DataFrame(data).copy(), self.coord_cols)
        return self.return_df(df, self.to_dict)


    def convert_degrees_str(self, df: pd.DataFrame, coord_cols: list[str], sep=' ')->pd.DataFrame:  
        if df.empty:
            return df
        for col in tqdm(coord_cols, desc='Transforming coords'):
            cardinal_col = f'CARDINAL_{col}'
            df[col] = df[col].str.strip()
            df[[col, cardinal_col]] = df[col].str.split(' ', expand=True)
            df[col] = pd.to_numeric(df[col], errors='coerce')
            df[cardinal_col] = df[cardinal_col].str.lower()
            df[cardinal_col] = df[cardinal_col].replace(self.cardinal_signs)
            df[cardinal_col] = pd.to_numeric(df[cardinal_col], errors='coerce')
            df[col] = df[col]*df[cardinal_col]
            df.drop(cardinal_col, axis=1, inplace=True)
        return df


class BadCoordsIdentifier(BaseIngestionTransformer):
    """
    Flags coordinates that fall outside the passed limits.
    """
    def __init__(self, **params):
        self.lat_lon_cols = params.get('lat_long_cols', [])
        self.to_dict = params.get('to_dict', False)
        self.clean_data = params.get('clean_data', False)
        self.lat_lon_limits = {self.lat_lon_cols[0]: [-90, 90], self.lat_lon_cols[1]: [-180, 180]}


    def transform(self, data: Union[pd.DataFrame, list[dict]]):
        df = pd.DataFrame(data)
        if df.empty:
            return df

        df = pd.DataFrame(data).copy()
        for col in tqdm(self.lat_lon_limits, desc='Identifying bad coords'):
            df[f'FLAG_{col}'] = ~df[col].between(*self.lat_lon_limits[col])
            if self.clean_data:
                df = df[~df[f'FLAG_{col}']].reset_index(drop=True)

        return self.return_df(df, self.to_dict)



        



        