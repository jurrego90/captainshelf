import pandas as pd
import numpy as np
from .function_applier import FuncApplier





class HeadingAngleCalculator(FuncApplier):
    """
    Implement the heading calculation 
    """
    def __init__(self, **params):
        params = {'func': heading_angle_format, 'func_params':params}
        super().__init__(**params)



def heading_angle_array(lat1, lon1, lat2, lon2):
    """
    Extension of heading_angle that can be applied to arrays.

    Parameters:
    -----------
    lat1 (np.array[float]): the degree of latitude of the first point
    lon1 (np.array[float]): the degree of longitude of the first point
    lat2 (np.array[float]): the degree of latitude of the second point
    lon2 (np.array[float]): the degree of longitude of the second point

    Returns:
       (np.array[float]): heading angle in degree
    """
    lat1, lat2 = np.radians(lat1), np.radians(lat2)
    lon1, lon2 = np.radians(lon1), np.radians(lon2)
    dLon = lon2 - lon1
    x = np.sin(dLon) * np.cos(lat2)
    y = np.cos(lat1) * np.sin(lat2) - np.sin(lat1) * np.cos(lat2) * np.cos(dLon)
    brng = np.rad2deg(np.arctan2(x, y))
    return (brng + 360) % 360


def heading_angle_format(df: pd.DataFrame, **params):
    """
    Hi
    """
    time_cols=params.get('time_cols', ['UTC']) 
    lat_lon_cols=params.get('lat_lon_cols', ['POSITION (LATITUDE)', 'POSITION (LONGITUDE)'])
    grouping_cols=params.get('grouping_cols', ['SHIP_ID', 'REPORT_TYPE'])
    df.sort_values(by=time_cols, ascending=False, inplace=True)
    heading_cols = grouping_cols +  time_cols + lat_lon_cols
    df_heading = df.copy()
    df_heading = df_heading[heading_cols]
    df_heading =df_heading[~df_heading.duplicated()].reset_index(drop=True)
    print('Calculating heading...', end='\r')
    lagged_lat_lon = {col: f'{col}_LAG_1' for col in lat_lon_cols}
    for col in lat_lon_cols:
        df_heading[lagged_lat_lon[col]] = df_heading.groupby(grouping_cols)[col].shift(-1).values
        df_heading[lagged_lat_lon[col]].fillna(df[col], inplace=True)

    lat1 = df_heading[lagged_lat_lon[lat_lon_cols[0]]]
    lon1 = df_heading[lagged_lat_lon[lat_lon_cols[1]]]
    lat2 = df_heading[lat_lon_cols[0]]
    lon2 = df_heading[lat_lon_cols[1]]
    df_heading['heading'] = heading_angle_array(lat1, lon1, lat2, lon2 )
    df_heading.drop(lagged_lat_lon.values(), axis=1, inplace=True)
    df_heading = df.merge(df_heading, 'left', heading_cols)
    print('Calculating heading...Done')
    return df_heading